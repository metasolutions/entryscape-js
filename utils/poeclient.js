module.exports = function(api_token, projectId) {

    var request = require("request");
    var q = require("q");

    var api_url = "https://poeditor.com/api/";

    function log(msg) {
        console.log(msg);
    }

    function getURLForLanguage(lang) {
        var deferred = q.defer();
        var options = {
            url: api_url,
            method: "POST",
            form: {
                action: "export",
                api_token: api_token,
                id: projectId,
                language: lang,
                type: "json",
                filters: ["translated"]
            }
        };
        request(options, function (error, response, body) {
            if (error) {
                deferred.reject(error);
            } else {
                deferred.resolve(JSON.parse(body).item);
            }
        });
        return deferred.promise;
    }

    function exportTermsAndDefinitions(lang) {
        return getURLForLanguage(lang).then(function (url) {
            if (url && url.length > 0) {
                var deferred = q.defer();
                request(url, function (error, response, body) {
                    if (error) {
                        deferred.reject(new Error(error));
                    } else {
                        deferred.resolve(JSON.parse(body));
                    }
                });
                return deferred.promise;
            }
        });
    }

    function listProjects() {
        var deferred = q.defer();
        var options = {
            url: api_url,
            method: "POST",
            form: {
                action: "list_projects",
                api_token: api_token
            }
        };
        request(options, function (error, response, body) {
            if (error) {
                deferred.reject(error);
            } else {
                deferred.resolve(JSON.parse(body).list);
            }
        });
        return deferred.promise;
    }

    function listProjectLanguages() {
        var deferred = q.defer();
        var options = {
            url: api_url,
            method: "POST",
            form: {
                action: "list_languages",
                api_token: api_token,
                id: projectId
            }
        };
        request(options, function (error, response, body) {
            if (error) {
                deferred.reject(error);
            } else {
                deferred.resolve(JSON.parse(body).list);
            }
        });
        return deferred.promise;
    }

    function syncTerms(terms) {
        var deferred = q.defer();
        var options = {
            url: api_url,
            method: "POST",
            form: {
                action: "sync_terms",
                api_token: api_token,
                id: projectId,
                data: JSON.stringify(terms)
            }
        };
        request(options, function (error, response, body) {
            if (error) {
                deferred.reject(error);
            } else {
                deferred.resolve(body);
            }
        });
        return deferred.promise;
    }

    function uploadDefinitions(defs, language) {
        var deferred = q.defer();
        var options = {
            url: api_url,
            formData: {
                action: "upload",
                api_token: api_token,
                id: projectId,
                updating: "definitions",
                overwrite: 1,
                sync_terms: 0,
                language: language,
                fuzzy_trigger: 1,
                file: {
                    value: JSON.stringify(defs),
                    options: {
                        filename: "termsdefs.json",
                        contentType: "application/json"
                    }
                }
            }
        };

        request.post(options, function (error, response, body) {
            if (error) {
                deferred.reject(error);
            } else {
                deferred.resolve(JSON.parse(body));
            }
        });

        return deferred.promise;
    }

    return {
        getURLForLanguage: getURLForLanguage,
        exportTermsAndDefinitions: exportTermsAndDefinitions,
        listProjects: listProjects,
        listProjectLanguages: listProjectLanguages,
        syncTerms: syncTerms,
        uploadDefinitions: uploadDefinitions
    }
};
