var q = require('q');

module.exports = function(grunt) {
    grunt.config.merge({
        availabletasks: {           // task
            tasks: {
                options: {
                    filter: 'exclude',
                    tasks: ['availabletasks', 'default'],
                    groups: {
                        'Localization tasks (for developers):': ['nls', 'nlsDev', 'nlsRel'],
                        'Localization tasks (for POEditor administrators):': ['nlsUpload', 'nlsUploadLang', 'nlsUploadTerms', 'nlsDownload'],
                        'The help tasks:': ['help'],
                        'Build tasks:': ['build', 'buildDev'],
                        'Install tasks': ['install', 'update'],
                        'Code quality': ['eslint']
                    },
                    hideUngrouped: true
                }
            }
        }
    });
//    grunt.loadNpmTasks('grunt-available-tasks');
    grunt.registerTask('default', ['availabletasks']);
    grunt.registerTask('help', 'Provides a list of available tasks', ['availabletasks']);
};