module.exports = function (grunt) {
  grunt.registerTask('buildDev', 'Development build, no install libraries or nls tasks are run', ['buildnlsconf', 'requirejs:js', 'requirejs:css']);
  grunt.registerTask('build', 'Complete build, includes install libraries and nls copy before' +
    ' building js and css files', ['install', 'nlsRel', 'buildfixes', 'polyfill', 'requirejs:js', 'requirejs:css', 'babl', 'uglify']);
};
