module.exports = function (grunt) {
  grunt.config.merge({
    copy: {
      polyfill: {
        files: [
          {
            expand: true,
            src: '**',
            dest: 'libs/babel-polyfill',
            cwd: 'node_modules/babel-polyfill',
          },
          {
            expand: true,
            src: 'formdata.min.js',
            dest: 'libs/formdata-polyfill',
            cwd: 'node_modules/formdata-polyfill',
          },
        ],
      },
    },
  });

  require('load-grunt-tasks')(grunt);
  grunt.registerTask('polyfill', ['copy']);
};
