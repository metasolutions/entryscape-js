module.exports = function (grunt) {
  grunt.config.merge({
    eslint: {
      options: {
        quiet: true,
        configFile: 'config/js/.eslintrc.js',
      },
      src: [
        '**/*.js',
        '!nls/translations/**/*.js',
        '!nls/merged/**/*.js',
        '!libs/**/*.js',
        '!node_modules/**/*.js',
        '!release/**/*.js',
        '!config/**/*.js'],
    },
  });
};
