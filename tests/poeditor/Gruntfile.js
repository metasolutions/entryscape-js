module.exports = function(grunt) {

    var POEClient = require("../../utils/poeclient")("POEDITOR_API_TOKEN");

    function log(msg) {
        grunt.log.writeln(msg);
    }

    grunt.registerTask('poe_export_test', function() {
        var done = this.async();
        POEClient.exportTermsAndDefinitions("83821", "en").then(function(terms) {
            terms.forEach(function(t) {
                log(t.term + ": " + t.definition);
            });
            done();
        }, function(err) {
            log(err);
            done(false);
        });
    });

    grunt.registerTask('poe_upload_test', function() {
        var done = this.async();
        POEClient.uploadDefinitions("83821", [{"term":"Sign in to EntryScape XXX","definition":"hepp","context":"signin","term_plural":"","reference":"thisisareference","comment":"En kommentar"},{"term":"Sign out of EntryScape","definition":null,"context":"signout","term_plural":"","reference":"","comment":""},{"term":"Terminologies","definition":null,"context":"modules","term_plural":"","reference":"","comment":""},{"term":"One key","definition":null,"context":"","term_plural":"","reference":"nyckel_bla","comment":""},{"term":"New term","definition":null,"context":"","term_plural":"","reference":"","comment":""},{"term":"gurkapurka","definition":null,"context":"ggg","term_plural":"","reference":"headerLabel","comment":""},{"term":"gurkapurka","definition":null,"context":"aaa","term_plural":"","reference":"okLabel","comment":""}], "de").then(function(result) {
            done();
        }, function(err) {
            log(err);
            done(false);
        });
    });

    grunt.registerTask('poe_sync_test', function() {
        var done = this.async();
        POEClient.syncTerms("83821", [{"term":"Sign in to EntryScape"},{"term":"Sign out of EntryScape"},{"term":"Terminologies"},{"term":"One key"},{"term":"New term"},{"term":"gurkapurka"},{"term":"gurkapurka"}]).then(function(result) {
            done();
        }, function(err) {
            log(err);
            done(false);
        });
    });

    grunt.registerTask('poe_listprojects_test', function() {
        var done = this.async();
        POEClient.listProjects().then(function(projects) {
            projects.forEach(function(p) {
                log(p.id + ": " + p.name);
            });
            done();
        }, function(err) {
            log(err);
            done(false);
        });
    });

    grunt.registerTask('poe_listlanguages_test', function() {
        var done = this.async();
        POEClient.listProjectLanguages("83821").then(function(langs) {
            langs.forEach(function(l) {
                log(l.code + ": " + l.name);
            });
            done();
        }, function(err) {
            log(err);
            done(false);
        });
    });

    grunt.registerTask('default', ['poe_export_test']);

};